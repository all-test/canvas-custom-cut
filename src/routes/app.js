import React from 'react';
// import NProgress from 'nprogress';
import PropTypes from 'prop-types';
// import pathToRegexp from 'path-to-regexp';
import { connect } from 'dva';
import { Layout, Menu } from 'antd';
import { withRouter, Link } from 'dva/router';
// import styles from './app.less';
import MLayout from '../components/Layout';

const { Header, Content, Footer, Sider } = Layout;

const App = ({ children, dispatch, app, loading, location }) => {
	return <MLayout children={children} />;
};

App.propTypes = {
	children: PropTypes.element.isRequired,
	location: PropTypes.object,
	dispatch: PropTypes.func,
	app: PropTypes.object,
	loading: PropTypes.object
};

export default withRouter(connect(({ app, loading }) => ({ app, loading }))(App));
