import React, { Component } from 'react';
import { connect } from 'dva';
import { message, Upload, Button } from 'antd';
import styles from './_index.less';
import { getElementLeft, getElementTop } from '../../utils/tools';
function tips(type, title) {
	message.destroy();
	message[type](title);
}

class IndexPage extends Component {
	constructor(props) {
		super(props);
		this.cs;
		this.ctxs;
		this.cc;
		this.ctxc;
		this.state = {
			dotNumber: 1,
			isCut: false,
			pointList: []
		};
	}

	componentDidMount() {
		this.cs = document.getElementById('jkCanvasShow');
		this.ctxs = this.cs.getContext('2d', { alpha: false });
		this.cc = document.getElementById('jkCanvasCut');
		this.ctxc = this.cc.getContext('2d');
	}

	upload = (info) => {
		const file = info.file;
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			this.drawToCanvas(reader.result);
		};
	};

	drawToCanvas = (imageData) => {
		let img = new Image();
		img.src = imageData;
		img.onload = () => {
			this.cs.width = img.naturalWidth;
			this.cs.height = img.naturalHeight;
			this.ctxs.drawImage(img, 0, 0, this.cs.width, this.cs.height);

			this.cc.width = this.cs.width;
			this.cc.height = this.cs.height;
			// this.ctxc.fillStyle = 'rgba(255,255,255,.2)';
			// this.ctxc.fillRect(0, 0, this.cs.width, this.cs.height);
		};
	};

	// cut = () => {
	// 	this.setState(
	// 		{
	// 			isCut: true
	// 		},
	// 		() => {
	// 			this.cc = document.getElementById('jkCanvasCut');
	// 			this.ctxc = this.cc.getContext('2d');
	// 			this.cc.width = this.cs.width;
	// 			this.cc.height = this.cs.height;
	// 			this.ctxc.beginPath();

	// 			this.ctxc.fillStyle = 'rgba(255,255,255,.2)';
	// 			this.ctxc.fillRect(0, 0, this.cs.width, this.cs.height);
	// 		}
	// 	);
	// };
	renderModal = () => {
		this.ctxc.fillStyle = 'rgba(255,255,255,.2)';
		this.ctxc.fillRect(0, 0, this.cs.width, this.cs.height);
  };
  renderFour = (pointList) => {
    this.ctxc.beginPath();
			this.renderModal();
			this.ctxc.fillStyle = 'yellow';
			this.ctxc.strokeStyle = 'red';

			this.ctxc.arc(pointList[0].x, pointList[0].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			this.ctxc.moveTo(pointList[1].x + 4, pointList[1].y);

			this.ctxc.arc(pointList[1].x, pointList[1].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[1].x, pointList[1].y);
			this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			this.ctxc.moveTo(pointList[2].x + 4, pointList[2].y);

			this.ctxc.arc(pointList[2].x, pointList[2].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[2].x, pointList[2].y);
			this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			this.ctxc.moveTo(pointList[3].x + 4, pointList[3].y);
			this.ctxc.arc(pointList[3].x, pointList[3].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[3].x, pointList[3].y);
			this.ctxc.lineTo(pointList[0].x, pointList[0].y);
			this.ctxc.fill();
			this.ctxc.stroke();

			this.ctxc.fillStyle = 'transparent';
			this.ctxc.globalCompositeOperation = 'destination-in';
			this.ctxc.beginPath();
			this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			this.ctxc.closePath();
			this.ctxc.fill();
			this.ctxc.clip();
  }

	customCut = (e) => {
		const { dotNumber, pointList } = this.state;

		let mouseX = e.clientX - getElementLeft(this.cc);
		let mouseY = e.clientY - getElementTop(this.cc);
		let dotObj = {};

		if (dotNumber <= 4) {
			dotObj.x = mouseX;
			dotObj.y = mouseY;
			pointList.push(dotObj);
			this.setState({
				dotNumber: dotNumber + 1
			});
		} else {
			return;
		}

		if (dotNumber == 1) {
			this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);
			this.ctxc.beginPath();
			this.renderModal();
			this.ctxc.fillStyle = 'yellow';
			this.ctxc.strokeStyle = 'red';
			this.ctxc.arc(pointList[0].x, pointList[0].y, 4, 0, 2 * Math.PI);
			this.ctxc.fill();
			this.ctxc.stroke();
		} else if (dotNumber == 2) {
			this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);
			this.ctxc.beginPath();
			this.renderModal();
			this.ctxc.fillStyle = 'yellow';
			this.ctxc.strokeStyle = 'red';
			this.ctxc.arc(pointList[0].x, pointList[0].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			this.ctxc.moveTo(pointList[1].x + 4, pointList[1].y);
			this.ctxc.arc(pointList[1].x, pointList[1].y, 4, 0, 2 * Math.PI);
			this.ctxc.fill();
			this.ctxc.stroke();
		} else if (dotNumber == 3) {
			this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);
			this.ctxc.beginPath();
			this.renderModal();
			this.ctxc.fillStyle = 'yellow';
			this.ctxc.strokeStyle = 'red';
			this.ctxc.arc(pointList[0].x, pointList[0].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			this.ctxc.moveTo(pointList[1].x + 4, pointList[1].y);
			this.ctxc.arc(pointList[1].x, pointList[1].y, 4, 0, 2 * Math.PI);
			this.ctxc.moveTo(pointList[1].x, pointList[1].y);
			this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			this.ctxc.moveTo(pointList[2].x + 4, pointList[2].y);
			this.ctxc.arc(pointList[2].x, pointList[2].y, 4, 0, 2 * Math.PI);
			this.ctxc.fill();
			this.ctxc.stroke();
		} else if (dotNumber == 4) {
      this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);
      this.renderFour(pointList)
			// this.ctxc.beginPath();
			// this.renderModal();
			// this.ctxc.fillStyle = 'yellow';
			// this.ctxc.strokeStyle = 'red';

			// this.ctxc.arc(pointList[0].x, pointList[0].y, 4, 0, 2 * Math.PI);
			// this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			// this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			// this.ctxc.moveTo(pointList[1].x + 4, pointList[1].y);

			// this.ctxc.arc(pointList[1].x, pointList[1].y, 4, 0, 2 * Math.PI);
			// this.ctxc.moveTo(pointList[1].x, pointList[1].y);
			// this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			// this.ctxc.moveTo(pointList[2].x + 4, pointList[2].y);

			// this.ctxc.arc(pointList[2].x, pointList[2].y, 4, 0, 2 * Math.PI);
			// this.ctxc.moveTo(pointList[2].x, pointList[2].y);
			// this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			// this.ctxc.moveTo(pointList[3].x + 4, pointList[3].y);
			// this.ctxc.arc(pointList[3].x, pointList[3].y, 4, 0, 2 * Math.PI);
			// this.ctxc.moveTo(pointList[3].x, pointList[3].y);
			// this.ctxc.lineTo(pointList[0].x, pointList[0].y);
			// this.ctxc.fill();
			// this.ctxc.stroke();

			// this.ctxc.fillStyle = 'transparent';
			// this.ctxc.globalCompositeOperation = 'destination-in';
			// this.ctxc.beginPath();
			// this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			// this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			// this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			// this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			// this.ctxc.closePath();
			// this.ctxc.fill();
			// this.ctxc.clip();

			this.cc.onmousemove = (e) => {
				let mouseX = e.clientX - getElementLeft(this.cc);
				let mouseY = e.clientY - getElementTop(this.cc);

				if (
					mouseX < pointList[0].x + 4 &&
					mouseX > pointList[0].x - 4 &&
					mouseY < pointList[0].y + 4 &&
					mouseY > pointList[0].y - 4
				) {
					document.onmousedown = () => {
						console.log('first');
						document.onmousemove = (e) => {
							let mouseX = e.clientX - getElementLeft(this.cc);
							let mouseY = e.clientY - getElementTop(this.cc);
							pointList[0].x = mouseX;
							pointList[0].y = mouseY;

              this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);

						};
					};
				}
			};

			// this.ctxc.moveTo(pointList[dotNumber - 2].x, pointList[dotNumber - 2].y);
			// this.ctxc.lineTo(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y);
			// this.ctxc.moveTo(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y);
			// this.ctxc.lineTo(pointList[0].x, pointList[0].y);
			// this.ctxc.stroke();

			// this.ctxc.fillStyle = 'transparent';
			// this.ctxc.globalCompositeOperation = 'destination-in';
			// this.ctxc.beginPath();
			// this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			// this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			// this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			// this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			// this.ctxc.closePath();
			// this.ctxc.clip();
			// this.ctxc.fill();

			// this.ctxs.globalCompositeOperation = 'destination-in';
			// this.ctxs.beginPath();
			// this.ctxs.moveTo(pointList[0].x, pointList[0].y);
			// this.ctxs.lineTo(pointList[1].x, pointList[1].y);
			// this.ctxs.lineTo(pointList[2].x, pointList[2].y);
			// this.ctxs.lineTo(pointList[3].x, pointList[3].y);
			// this.ctxs.closePath();
			// this.ctxs.fill();

			// this.cc.onmousemove = (e) => {
			// 	let mouseX = e.clientX - getElementLeft(this.cc);
			// 	let mouseY = e.clientY - getElementTop(this.cc);
			// 	console.log(mouseX, mouseY, 'aa');

			// 	if (
			// 		mouseX < pointList[0].x + 4 &&
			// 		mouseX > pointList[0].x - 4 &&
			// 		mouseY < pointList[0].y + 4 &&
			// 		mouseY > pointList[0].y - 4
			// 	) {
			// 		this.cc.style.cursor = 'move';
			// 		this.cc.onmousedown = () => {
			// 			document.onmousemove = (e) => {
			// 				let mouseX = e.clientX - getElementLeft(this.cc);
			// 				let mouseY = e.clientY - getElementTop(this.cc);
			// 				console.log(mouseX, mouseY, 'bb');
			// 				pointList[0].x = mouseX;
			// 				pointList[0].y = mouseY;

			// 				this.ctxc.clearRect(0, 0, this.cc.width, this.cc.height);
			// 				this.ctxc.beginPath();

			// 				this.ctxc.fillStyle = 'yellow';
			// 				this.ctxc.strokeStyle = 'red';
			// 				this.ctxc.beginPath();

			// 				pointList &&
			// 					pointList.map((item) => {
			// 						console.log(item.x, item.y);
			// 						this.ctxc.arc(item.x, item.y, 2, 0, 2 * Math.PI);
			// 						this.ctxc.stroke();
			// 						this.ctxc.fill();
			// 					});

			// 				// console.log(pointList[0].x);
			// 				// this.ctxc.fillStyle = 'red';
			// 				// this.ctxc.globalCompositeOperation = 'destination-in';
			// 				// this.ctxc.beginPath();
			// 				// this.ctxc.moveTo(pointList[0].x, pointList[0].y);
			// 				// this.ctxc.lineTo(pointList[1].x, pointList[1].y);
			// 				// this.ctxc.lineTo(pointList[2].x, pointList[2].y);
			// 				// this.ctxc.lineTo(pointList[3].x, pointList[3].y);
			// 				// this.ctxc.closePath();
			// 				// this.ctxc.clip();
			// 				// this.ctxc.fill();
			// 				// this.ctxc.stroke()
			// 			};
			// 			document.onmouseup = () => {
			// 				document.onmousedown = null;
			// 				document.onmousemove = null;
			// 			};
			// 		};
			// 	} else if (
			// 		mouseX < pointList[1].x + 4 &&
			// 		mouseX > pointList[1].x - 4 &&
			// 		mouseY < pointList[1].y + 4 &&
			// 		mouseY > pointList[1].y - 4
			// 	) {
			// 		this.cc.style.cursor = 'move';
			// 		this.cc.onmousedown = () => {
			// 			document.onmousemove = (e) => {
			// 				let mouseX = e.clientX - getElementLeft(this.cc);
			// 				let mouseY = e.clientY - getElementTop(this.cc);
			// 				console.log(mouseX, mouseY, 'bb');
			// 			};
			// 			document.onmouseup = () => {
			// 				document.onmousedown = null;
			// 				document.onmousemove = null;
			// 			};
			// 		};
			// 	} else if (
			// 		mouseX < pointList[2].x + 4 &&
			// 		mouseX > pointList[2].x - 4 &&
			// 		mouseY < pointList[2].y + 4 &&
			// 		mouseY > pointList[2].y - 4
			// 	) {
			// 		this.cc.style.cursor = 'move';
			// 		this.cc.onmousedown = () => {
			// 			document.onmousemove = (e) => {
			// 				let mouseX = e.clientX - getElementLeft(this.cc);
			// 				let mouseY = e.clientY - getElementTop(this.cc);
			// 				console.log(mouseX, mouseY, 'bb');
			// 			};
			// 			document.onmouseup = () => {
			// 				document.onmousedown = null;
			// 				document.onmousemove = null;
			// 			};
			// 		};
			// 	} else if (
			// 		mouseX < pointList[3].x + 4 &&
			// 		mouseX > pointList[3].x - 4 &&
			// 		mouseY < pointList[3].y + 4 &&
			// 		mouseY > pointList[3].y - 4
			// 	) {
			// 		this.cc.style.cursor = 'move';
			// 		this.cc.onmousedown = () => {
			// 			document.onmousemove = (e) => {
			// 				let mouseX = e.clientX - getElementLeft(this.cc);
			// 				let mouseY = e.clientY - getElementTop(this.cc);
			// 				console.log(mouseX, mouseY, 'bb');
			// 			};
			// 			document.onmouseup = () => {
			// 				document.onmousedown = null;
			// 				document.onmousemove = null;
			// 			};
			// 		};
			// 	} else {
			// 		this.cc.style.cursor = 'default';
			// 	}
			// };
		}
	};

	render() {
		const { isCut } = this.state;
		const props = {
			name: 'file',
			accept: 'image/*',
			multiple: false,
			showUploadList: false
		};
		return (
			<div>
				<div className={styles.jkUpload}>
					<Upload {...props} customRequest={(info) => this.upload(info)}>
						<Button>上传</Button>
					</Upload>
				</div>
				<div className={styles.jkCanvas}>
					<canvas id="jkCanvasShow">你的浏览器不支持canvas,请升级你的浏览器</canvas>
					{/* {isCut ? (
						<canvas id="jkCanvasCut" onClick={this.customCut}>
							你的浏览器不支持canvas,请升级你的浏览器
						</canvas>
          ) : null} */}
					<canvas id="jkCanvasCut" onClick={this.customCut}>
						你的浏览器不支持canvas,请升级你的浏览器
					</canvas>
				</div>
			</div>
		);
	}
}

export default connect()(IndexPage);
