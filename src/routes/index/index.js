import React, { Component } from 'react';
import { connect } from 'dva';
import { message, Upload, Button } from 'antd';
import styles from './index.less';
import { getElementLeft, getElementTop } from '../../utils/tools';
function tips(type, title) {
	message.destroy();
	message[type](title);
}

class IndexPage extends Component {
	constructor(props) {
		super(props);
		this.c;
		this.ctx;
		this.state = {
			dotNumber: 1,
			pointList: []
		};
	}

	componentDidMount() {
		this.c = document.getElementById('jkCanvas');
		this.ctx = this.c.getContext('2d', { alpha: false });
	}

	upload = (info) => {
		const file = info.file;
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			this.drawToCanvas(reader.result);
		};
	};

	drawToCanvas = (imageData) => {
		let img = new Image();
		img.src = imageData;
		img.onload = () => {
			this.c.width = img.naturalWidth;
			this.c.height = img.naturalHeight;
			this.ctx.drawImage(img, 0, 0, this.c.width, this.c.height);
		};
	};

	customCut = (e) => {
		const { dotNumber, pointList } = this.state;

		// console.log(e.clientX); //屏幕左侧到鼠标点击位置X坐标
		// console.log(getElementLeft(this.c));  //元素到屏幕左侧
		let mouseX = e.clientX - getElementLeft(this.c);
		let mouseY = e.clientY - getElementTop(this.c);
		let dotObj = {};

		if (dotNumber <= 4) {
			dotObj.x = mouseX;
			dotObj.y = mouseY;
			pointList.push(dotObj);
			this.setState({
				dotNumber: dotNumber + 1
			});
		} else {
			return;
		}

		this.ctx.beginPath();
		this.ctx.arc(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y, 4, 0, 2 * Math.PI);
		this.ctx.fillStyle = 'yellow';
		this.ctx.strokeStyle = 'red';
		this.ctx.fill();
		if (dotNumber == 1) {
			this.ctx.stroke();
		}
		if (dotNumber == 2 || dotNumber == 3) {
			this.ctx.moveTo(pointList[dotNumber - 2].x, pointList[dotNumber - 2].y);
			this.ctx.lineTo(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y);
			this.ctx.stroke();
		} else if (dotNumber == 4) {
			this.ctx.moveTo(pointList[dotNumber - 2].x, pointList[dotNumber - 2].y);
			this.ctx.lineTo(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y);
			this.ctx.moveTo(pointList[dotNumber - 1].x, pointList[dotNumber - 1].y);
			this.ctx.lineTo(pointList[0].x, pointList[0].y);
			this.ctx.stroke();
		}
	};

	render() {
		const props = {
			name: 'file',
			accept: 'image/*',
			multiple: false,
			showUploadList: false
		};
		return (
			<div>
				<div className={styles.jkUpload}>
					<Upload {...props} customRequest={(info) => this.upload(info)}>
						<Button>上传</Button>
					</Upload>
				</div>
				<div className={styles.jkCanvas}>
					<canvas ref="jkCanvas" id="jkCanvas" onClick={this.customCut}>
						你的浏览器不支持canvas,请升级你的浏览器
					</canvas>
				</div>
			</div>
		);
	}
}

export default connect()(IndexPage);
