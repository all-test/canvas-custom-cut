import '@babel/polyfill';
import 'url-polyfill';
import dva from 'dva';

import createHistory from 'history/createBrowserHistory';
import createLoading from 'dva-loading';
import router from './router';
import './index.less';


// 1. Initialize
const app = dva({
  ...createLoading({
    effects: true
  }),
  history: createHistory(),
});

// 2. Model 迁移到路由中
// app.model(require('./models/example'));

// 3. Router
app.router(router);

// 4. Start
app.start('#root');

export default app._store; // eslint-disable-line
