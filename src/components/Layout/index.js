import React, { Component } from 'react';
import { connect } from 'dva';
import { message, Layout } from 'antd';
import styles from './index.less';

const { Header, Footer, Content } = Layout;

function tips(type, title) {
	message.destroy();
	message[type](title);
}

class MLayout extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {}

	render() {
		return (
      <Layout>
        <Header>Cut-Test</Header>
				<Content id="content">{this.props.children}</Content>
			</Layout>
		);
	}
}

const mapStateToProps = (state) => {
	return {};
};

export default connect(mapStateToProps)(MLayout);
